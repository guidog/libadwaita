/*
 * Copyright (C) 2019 Alexander Mikhaylenko <exalm7659@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#include "config.h"
#include "adw-navigation-direction.h"

/**
 * SECTION:adw-navigation-direction
 * @short_description: Swipe navigation directions.
 * @title: AdwNavigationDirection
 * @See_also: #AdwLeaflet
 *
 * Since: 1.0
 */

/**
 * AdwNavigationDirection:
 * @ADW_NAVIGATION_DIRECTION_BACK: Corresponds to start or top, depending on orientation and text direction
 * @ADW_NAVIGATION_DIRECTION_FORWARD: Corresponds to end or bottom, depending on orientation and text direction
 *
 * Represents direction of a swipe navigation gesture in #AdwLeaflet.
 *
 * Since: 1.0
 */
